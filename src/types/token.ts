export type Token = string

export type Dataspace = {
  [name: string]: [content: string[]]
}

export type DecodedToken = {
  resource_access : {
    [key: string] : {
      roles: string[]
    }
  },
  groups: string[],

  /** @deprecated has been replaced by "dataspaces" */
  "fiware-services"?: Dataspace,

  // contains all data spaces
  "dataspaces": string[],

  // exp: string,
  // iat: string,
  // jti: string,
  // iss: string,
  // aud: string[],
  // sub: string,
  // typ: string,
  // azp: string,
  // session_state: string,
  // "allowed-origins": string[],
  // realm_access: {
  //   roles: string[]
  // },
  // scope: string,
  // sid: string,
  // email_verified: string,
  // name: string,
  // preferred_username: string,
  // given_name: string,
  // family_name: string,
  // email: string,
}