import axios from "axios";
import qs from "qs"
import jwt from "jsonwebtoken";
import type JwtPayload from "jsonwebtoken";
import { DecodedToken } from '../../types/token'
import Log from '../log'

/**
 * @class KeyCloak
 * @author Andreas Linneweber
 * 
 * @description
 * This class reaches out to Key cloak to check if the provided JTW is still valid, and if so decodes it and 
 * returns the decoded version
 * 
 * The login function is being used currently only for testing 
 */

export default class KeyCloak {
  constructor() {

  }

  // NOTE no need to type the response of this function since it's a temporary one
  async login() {
    const URL = process.env.KEYCLOAK_HOST + '/auth/realms/' + process.env.KEYCLOAK_REALM + '/protocol/openid-connect/token'
    const configurations = qs.stringify({
      grant_type: process.env.KEYCLOAK_GRANT_TYPE,
      client_id: process.env.KEYCLOAK_CLIENT_ID,
      client_secret: process.env.KEYCLOAK_CLIENT_SECRET,
      username: process.env.KEYCLOAK_USERNAME,
      password: process.env.KEYCLOAK_PASSWORD
    })

    Log.trace(configurations)

    const result = await axios({
      method: 'POST',
      url: URL,
      data: configurations,
      headers: { 'Content-type': 'application/x-www-form-urlencoded' },
      withCredentials: true,
    }).catch(reason => {
      Log.error("Code:", reason.response.status, reason.response.statusText,
        "| Error:", reason.response.data.error, "-", reason.response.data.error_description);
    });

    if (result) Log.trace(result)

    return result
  }

  /**
   * @description
   * This function checks, and if needed converts, the given signing key to a PEM format
   */
  private checkAndFormatKey(input: string): string {
    const pemHeader = "-----BEGIN PUBLIC KEY-----";
    const pemFooter = "-----END PUBLIC KEY-----";
    const formattedInput = input.replace(/\s/g, ""); // Remove all whitespace
  
    if (input.startsWith(pemHeader) && input.endsWith(pemFooter)) {
      // The input is already in PEM format
      return input;
    } else {
      // The input is not in PEM format
      const pemLines: string[] = [];
      for (let i = 0; i < formattedInput.length; i += 64) {
        // Split the input into 64 byte chunks
        pemLines.push(formattedInput.slice(i, i + 64));
      }
      // Concatenate the PEM lines
      const pemContent = pemLines.join("\n");
      // Add the PEM header and footer
      const pemFormattedString = `${pemHeader}\n${pemContent}\n${pemFooter}`;
      return pemFormattedString;
    }
  }


  async decodeToken(token: string): Promise<DecodedToken | undefined> {
    const URL = process.env.KEYCLOAK_HOST + '/auth/realms/' + process.env.KEYCLOAK_REALM + '/protocol/openid-connect/userinfo'

    try {
      await axios({
        method: 'GET',
        url: URL,
        headers: { Authorization: 'Bearer ' + token }
      })
    } catch (error) {
      Log.error('An error ocurred while decoding the authentication token')
      Log.debug(error)
    }

    Log.debug(token)

    if (!process.env.KEYCLOAK_PUBLIC_KEY || typeof process.env.KEYCLOAK_PUBLIC_KEY !== "string") {
      Log.error('No Keycloak Public Key set.')
      return undefined;
    }

    try {
      // Convert public key from raw to pem format

      const pemKey = this.checkAndFormatKey(process.env.KEYCLOAK_PUBLIC_KEY)

      const verifiedToken = jwt.verify(token, pemKey, { algorithms: ['RS256'] })

      if (typeof verifiedToken === "string") {
        Log.error('Token could not be verified. Unexpected format. Expected JwtPayload, got String.')
        return;
      }

      let decodedToken: DecodedToken = { dataspaces: [], resource_access : {}, groups: [], ...verifiedToken }
      if (decodedToken) {
        if (!Array.isArray(decodedToken.dataspaces)) {
          decodedToken.dataspaces = [];
        }

        if (decodedToken["fiware-services"] !== undefined) {
          let dataspaces: string[] = []
          for (const dataspace in decodedToken["fiware-services"]) {
            dataspaces.push(dataspace)
          }
          decodedToken.dataspaces = dataspaces;
        }

        Log.trace(decodedToken)

        return decodedToken
      }

    } catch (error) {
      Log.error('An error ocurred while verifying the authentication token')
      Log.debug(error)
      return undefined
    }
  }

}
