# Berlin Tegel - Masterportal Backend

The masterportal backend is a small backend that enables the static master portal configuration files to be filtered according to the necessary authorisations and then delivered filtered. 

## Architecture

The following pictures shows a very high level overview of the used architecture.

![Architecture](docs/overview.drawio.png)

### Background

The masterportal is a client side http Application, that runs completely in the browser. In the FUTR-HUB project the requirement of roles, that are needed for the displayed content in the portal, came up.

The project decided, to follow the following approach:

1. Implement a Login Addon as Addon for the Masterportal.
This addon was integrated in the masterportal core in the meantime.
2. Implement a backend, that uses the login information to filter the masterportal configuration
This backend is described and published in this repository.

### Overview

### Masterportal Login

The Login functionality is implemented as a tool in the masterportal. The code for the tool is part of the masterportal repository, the former addon was integrated in the core.

The login tool supports the OpenID Connect Login with an existing backend. We use Keycloak for this. 

The result of the login is, that the current user is authenticated and the OIDC Token is attached to every backend request (for a configurable domain) as Bearer token.

### Portal Backend

  The portal backend runs as a separate service in an own pod on our platform. The configuration files to filter must be provided over the file system.

The backend checks, if a bearer token is part of the request. 

If not: Only the content of an anonymous access role is delivered
If yes: All content, with matching roles of the user is delivered
(Important: the anonymous roles must be explicitly assigned to the user)

The role names are expected a specific claim in the token.

### Why this approach?

The implementation approach has the technical advantage, that the masterportal standard code can be used completely as is. The portal is even not aware of the running backend. 

## Functional Flow in Detail

The flow in total is as shown in the following sequence diagram:

![](docs/sequence_diagram.png)

### Anonymous Access

The steps from 1. to 5. are more or less the masterportal standard load of the page, except the facts, that the three configuration files ```config.json```, ```service.json``` and ```rest-service.json```are delivered by the backend. For the portal, the backend acts as a standard webserver.

### Authenticated Access

In the loaded masterportal, the user clicks "Login" (step 1). With this done, the portal opens a popup and in this popup the the redirects to the IDM (step 2) takes place.

The IDM handles the Authentication with the user. If the Login is successful, a the user is redirected back (step 9) to the portal and the Login Addon extracts a valid Access Token from the flow. 

After extracting the token, the portal is reloaded (step 10). The configuration files are requested (step 11) from the backend, too. In this case with the generated token in the ``Àuthorization`` header as Bearer token.
(If the user is not logged in, the portal reloads as described in the Section "Anonymous Access".)

The backend validates the token (step 12). If the token is valid, the the claim with the roles is extracted. After that, the three configuration files are filtered according to the roles. 
(the ```config.json``` is partly filtered directly (e.g. for tools)). The "Themetree" is filtered based on the before filtered service.json. Only entries, where the corresponding service configuration is still part of the filtered ```service.json``` are delivered. If folders get empty after the filtering, the folder is removed, too.)

The results are returned to the portal (step 13).

The results are rendered by the portal and shown to the user.

## Configuration and installation

To make this flow working, some configuration tasks are needed in three components.

### Requirements to the IDM

The IDM Configuration needs to following:

Due to the fact, that the masterportal is a browser based application, we created a new client in keycloak for the login process, which needs no authentication from the portal itself. 

The client needs to be:
* OpenID Connect Client with type "public access"
* The client has to have to correct redicrect URL for your masterportal hosting
(We normally use the Base URL of the Masterportal Server with a wildcard at the end of the URL.)
* You have to configure somewhere in your keycloak roles or groups, which can be assigned to users and are used as roles for accessing dataspaces
(The easiest way is to use the client roles of the new client and assign these to the users)
* Finally you have to configure client scopes, which add the roles as the claim "dataspaces" (can be configured) to the token. In Keycloak this can be done by using a client-role-mapper in the dedicated scopes of the client (add the claim to the access token)
The result in the token looks like this:
```json
    "given_name": "Andreas",
    "family_name": "Linneweber",
    "email": "andreas.linneweber@kernblick.de",
    "dataspaces": [
        "ds_aerial",
        "ds_intern",
        "ds_nda",
        "ds_protected"
    ]
```
Make sure this work, before you proceed with the next steps. In Keycloak this can be easily checked by using the Evaluation functions for the client scopes.

### Configuration of the Masterportal

The addon allows the user to login with an OIDC server. The retrieved access token is stored in cookies which can be used by the backend to deliver user-specific data (e.g. layers).

#### Installation and Configuration

To activate the addon in the masterportal, add the addon to your `config.js` as follows:

```js
const Config = {
  addons: ["login"],
  login: {
      oidcAuthorizationEndpoint: "https://idm.DOMAIN.de/auth/realms/REALM/protocol/openid-connect/auth",
      oidcTokenEndpoint: "https://idm.DOMAIN.de/auth/realms/REALM/protocol/openid-connect/token",
      oidcClientId: "masterportal",
      oidcRedirectUri: "https://localhost/portal/basic/",
      interceptorUrlRegex: "https?://localhost.*" // add authorization to all URLs that match the given regex
  },

  ...
```

Adjust the settings as required.

Finally, add the login tool to the `tools.children` section in the `config.json`:

```json
  "login": {
    "name": "translate#additional:modules.tools.login.title",
    "icon": "bi-door-open"
  }
```

Make these changes in your standard masterportal deployment. When you deploy these and open your masterportal, the Login function should be available in the tools menu.

When this works, proceed with the next step.

### Configuration of the Backend

To run the backend, the easiest way is using a docker image based on the Dockerfile in this repository.

Before, you have to copy the `config.json`, `service.json` and `rest-service.json` to input folder of this repository. After that, run ```docker build . -t portal-backend``` in the main folder of this repository.

The run the backend some configuration parameters are needed, these must be set as Environment Variables.

These parameters are are available in total:
```bash
PORT=8101 # Standard Port for the Backend, do not change when using docker
LOG_LEVEL=DEBUG # Log level for the Backend

TEST_MODE=TRUE # If true, the backend provides the ability to login itself. The next two Variables have to be set for this to work.
KEYCLOAK_USERNAME=<username_to_be_used_for_testing>
KEYCLOAK_PASSWORD=<password_to_be_used_for_testing>

#Keycloak configurations
KEYCLOAK_HOST=https://idm.<domain> # IDM host base URL, can be diffrent from keycloak
KEYCLOAK_GRANT_TYPE=password # grant type, keep the same
KEYCLOAK_REALM=<idm_realm> # IDM Realm name
KEYCLOAK_CLIENT_ID=<client_id> # IDM Client ID
KEYCLOAK_CLIENT_SECRET=<client_secret> # IDM Client Secret, can be emtpy with public clients. For more complex scenarios, you can provide the client credentials here
KEYCLOAK_PUBLIC_KEY="<idm_key>" # Client from IDM, which is used to sign the token(s)

PUBLIC_ROLE=<roleName> # The role name, which is used to access the public dataspaces
COOKIE_TOKEN_NAME=token # The name of the cookie, which is used to store the token

#File path
INPUT_FILE_PATH=input # Path to the input files - keep unchanged
SERVICE_INTERNET_INPUT_FILE=services # Name of the input file, services -> services.json
REST_SERVICES_INTERNET_INPUT_FILE=rest-services # Name of the input file, rest-services -> rest-services.json
CONFIG_INPUT_FILE=config # Name of the input file, services -> services.json, config -> config.json
INPUT_FILES_EXTENTION=.json # Extension of the input files
OUTPUT_FILES_EXTENTION=.json # Extension of the input files
OUTPUT_FILE_PATH=output # Extension of the input files
```

If you run the backend locally, you can provide these variables using an .env file.

For the docker version, use the follwing command:

```bash
docker run -d --name portal-backend \
  -p 8081:8101 \
  -e PORT=8101 \
  -e LOG_LEVEL=DEBUG \
  -e TEST_MODE=TRUE \
  -e KEYCLOAK_USERNAME=username_to_be_used_for_testing \
  -e KEYCLOAK_PASSWORD=password_to_be_used_for_testing \
  -e KEYCLOAK_HOST=https://idm.<domain> \
  -e KEYCLOAK_GRANT_TYPE=password \
  -e KEYCLOAK_REALM=idm_realm \
  -e KEYCLOAK_CLIENT_ID=client_id \
  -e KEYCLOAK_CLIENT_SECRET=client_secret \
  -e KEYCLOAK_PUBLIC_KEY="idm_key" \
  -e PUBLIC_ROLE=roleName \
  -e COOKIE_TOKEN_NAME=token \
  -e INPUT_FILE_PATH=input \
  -e SERVICE_INTERNET_INPUT_FILE=services \
  -e REST_SERVICES_INTERNET_INPUT_FILE=rest-services \
  -e CONFIG_INPUT_FILE=config \
  -e INPUT_FILES_EXTENTION=.json \
  -e OUTPUT_FILES_EXTENTION=.json \
  -e OUTPUT_FILE_PATH=output \
  portal-backend

```

After that, the backend should be up and running. You can test this by calling http://localhost:8101/config.json.

If this all works, open the ```config.js``` of your masterportal and change the following lines:

```json
    layerConf: "http://localhost:8101/services.json",
    restConf: "http://localhost:8101/rest-services.json",
    portalConf: "http://localhost:8101/config.json",
```

When you now start the portal, you can login and the backend will deliver the configuration files to the portal.